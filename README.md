## Inicializar o Projeto

### DB

Para comercar o projeto e necessario que a tabela com os campos corresponde esteja criada. 
Criei um arquivo e um script no Node para facilitar.

Rode o comando:

```javascript
    npm run db
```
ou 

```sql
    create table if not exists client
    (
        id                 bigint auto_increment
            constraint `PRIMARY`
            primary key,
        id_usuario         bigint                             not null,
        data_hora_cadastro datetime default CURRENT_TIMESTAMP not null,
        codigo             varchar(50)                        null,
        nome               varchar(150)                       not null,
        cpf_cnpj           varchar(20)                        not null,
        cep                int                                not null,
        logradouro         varchar(100)                       not null,
        endereco           varchar(120)                       not null,
        bairro             varchar(50)                        not null,
        cidade             varchar(60)                        null,
        uf                 varchar(2)                         null,
        complemento        varchar(150)                       not null,
        fone               varchar(15)                        not null,
        limite_credito     float                              not null,
        validade           date                               not null,
        numero             varchar(20)                        not null
    );

```

Este comando criara a tabela para o funcionamento das rotas. E necessario trocar as credencias para fazer as queries no banco.

O arquivo `database/client` e responsavel por isso:

```javascript
const DB = mysql.createConnection({
    host: '',
    user: '',
    password: '',
    database: ''
})
```
### Dependencias

Depois de realizado a criacao da tabela , instale as dependencias no backend e no frontend.

```javascript
    npm install
```

### UP

Inicialize o backend primeiro com o comando:

```javascript
    npm run start
```
E logo pode dar start no frontend rodando:

```javascript
    npm run serve
```
### Adendo

Um destaque de grande importancia a versao do *Node* ultlizada no projeto:`16.20`, ultilizar outra versao podera nao fazer as coisas funcionarem como esperado