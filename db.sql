create table if not exists client
(
    id                 bigint auto_increment
        constraint `PRIMARY`
        primary key,
    id_usuario         bigint                             not null,
    data_hora_cadastro datetime default CURRENT_TIMESTAMP not null,
    codigo             varchar(50)                        null,
    nome               varchar(150)                       not null,
    cpf_cnpj           varchar(20)                        not null,
    cep                int                                not null,
    logradouro         varchar(100)                       not null,
    endereco           varchar(120)                       not null,
    bairro             varchar(50)                        not null,
    cidade             varchar(60)                        null,
    uf                 varchar(2)                         null,
    complemento        varchar(150)                       not null,
    fone               varchar(15)                        not null,
    limite_credito     float                              not null,
    validade           date                               not null,
    numero             varchar(20)                        not null
);
