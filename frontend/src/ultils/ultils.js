export function formatCPF(cpf) {
    cpf = cpf.replace(/\D/g, '');

    if (cpf.length > 3) {
        cpf = cpf.replace(/^(\d{3})(\d)/, '$1.$2');
    }

    if (cpf.length > 6) {
        cpf = cpf.replace(/^(\d{3})\.(\d{3})(\d)/, '$1.$2.$3');
    }

    if (cpf.length > 9) {
        cpf = cpf.replace(/^(\d{3})\.(\d{3})\.(\d{3})(\d)/, '$1.$2.$3-$4');
    }

    return cpf
}