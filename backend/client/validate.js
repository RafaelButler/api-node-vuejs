const {body, param} = require('express-validator');


class ClientValidate {
    static reqBody() {
        return [
            body('nome').isString().notEmpty(),
            body('id_usuario').isInt().notEmpty(),
            body('codigo').isString().notEmpty(),
            body('cpf_cnpj').isString().notEmpty(),
            body('cep').isString().notEmpty(),
            body('logradouro').isString().notEmpty(),
            body('endereco').isString().notEmpty(),
            body('bairro').isString().notEmpty(),
            body('cidade').isString().notEmpty(),
            body('uf').isString().notEmpty(),
            body('complemento').isString(),
            body('fone').isString().notEmpty(),
            body('limite_credito').isFloat().notEmpty(),
            body('validade').isString().notEmpty(),
            body('numero').isInt().notEmpty(),
        ]
    }

    static requestParam() {
        return param('id').notEmpty().isString()
    }
}

module.exports = {ClientValidate}