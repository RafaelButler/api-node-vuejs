const {DB} = require("../database/client");

class UserClient {
    async get() {
        const [rows] = await (await DB).execute("SELECT * FROM client");
        return rows
    }

    async save(clientData) {
        const {
            id_usuario,
            codigo,
            nome,
            cpf_cnpj,
            cep,
            logradouro,
            endereco,
            bairro,
            cidade,
            uf,
            complemento,
            fone,
            limite_credito,
            validade,
            numero
        } = clientData
        const [rows] = await (await DB).execute("INSERT INTO client (id_usuario,codigo,nome,cpf_cnpj,cep,logradouro,endereco,bairro,cidade,uf,complemento,fone,limite_credito,validade,numero) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
            id_usuario,
            codigo,
            nome,
            cpf_cnpj,
            cep,
            logradouro,
            endereco,
            bairro,
            cidade,
            uf,
            complemento,
            fone,
            limite_credito,
            validade,
            numero
        ]);
        return clientData;
    }

    async show(id) {
        const [rows] = await (await DB).execute('SELECT * FROM client WHERE `id` = ?', [id]);
        return rows;
    }

    async delete(id) {
        const [rows] = await (await DB).execute('DELETE FROM client WHERE `id` = ?', [id]);
        return rows
    }

    async edit(id, clientData) {
        const {
            id_usuario,
            codigo,
            nome,
            cpf_cnpj,
            cep,
            logradouro,
            endereco,
            bairro,
            cidade,
            uf,
            complemento,
            fone,
            limite_credito,
            validade,
            numero
        } = clientData
        const [rows] = await (await DB).execute('UPDATE client SET `id_usuario` = ?, `codigo` = ?, `nome` = ?, `cpf_cnpj` = ?, `cep` = ?, `logradouro` = ?, `endereco` = ? ,`bairro` = ?, `cidade` = ?, `uf` = ?, `complemento` = ?, `fone` = ?, `limite_credito` = ?, `validade` = ?, `numero` = ? WHERE `id` = ?', [
            id_usuario,
            codigo,
            nome,
            cpf_cnpj,
            cep,
            logradouro,
            endereco,
            bairro,
            cidade,
            uf,
            complemento,
            fone,
            limite_credito,
            validade,
            numero,
            id
        ]);
        return clientData;
    }
}

module.exports = {UserClient}