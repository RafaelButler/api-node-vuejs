const {DB} = require("./client");

(async function up() {
    await (await DB).execute(/* SQL */
        "CREATE TABLE IF NOT EXISTS client (\n" +
        "    id                 bigint auto_increment,\n" +
        "    id_usuario         bigint                 not null,\n" +
        "    data_hora_cadastro datetime default now() not null,\n" +
        "    codigo             varchar(50)            null,\n" +
        "    nome               varchar(150)           not null,\n" +
        "    cpf_cnpj           varchar(20)            not null,\n" +
        "    cep                integer                not null,\n" +
        "    logradouro         varchar(100)           not null,\n" +
        "    endereco           varchar(120)           not null,\n" +
        "    bairro             varchar(50)            not null,\n" +
        "    cidade             varchar(60)            null,\n" +
        "    uf                 varchar(2)             null,\n" +
        "    complemento        varchar(150)           not null,\n" +
        "    fone               varchar(15)            not null,\n" +
        "    limite_credito     float                  not null,\n" +
        "    validade           date                   not null,\n" +
        "    numero             varchar(20)            not null,\n" +
        "    constraint client_pk\n" +
        "        primary key (id)\n" +
        ")"
    )

    await (await DB).end()
})();