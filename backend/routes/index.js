var express = require('express');
const {body, validationResult} = require('express-validator');
const {UserClient} = require("../client/user-client");
const {ClientValidate} = require("../client/validate");
var router = express.Router();

const userClient = new UserClient();

/* GET all clients */
router.get('/', async function (req, res, next) {
    const clients = await userClient.get();
    return res.end(JSON.stringify(clients)).status(201);
});

/* Save clients */
router.post('/', ClientValidate.reqBody(), async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.end(JSON.stringify(errors)).status(400)
    }

    const {body} = req;
    const client = await userClient.save(body);
    res.end(JSON.stringify(client))
});

/* Get only one client */
router.get('/client/:id', async function (req, res, next) {
    const client = await userClient.show(req.params.id);

    if (client.length <= 0) {
        return res.end(JSON.stringify({error: "User not found"})).status(204);
    }

    return res.end(JSON.stringify(client)).status(200);
});

/* Edit Clients */
router.put('/client/:id', ClientValidate.reqBody(), async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.end(JSON.stringify(errors)).status(400)
    }

    const {body} = req;
    const client = await userClient.edit(req.params.id, body);
    res.end(JSON.stringify(client)).status(200);
});

/* Delete Client */
router.delete('/client/:id', async function (req, res, next) {
    const client = await userClient.delete(req.params.id);

    if (client.length <= 0) {
        return res.end(JSON.stringify({error: "User not found"})).status(204);
    }

    return res.end(JSON.stringify({success: `User ${req.params.id} deleted successfully`})).status(200);
});

module.exports = router;
